import { AfStoreDoc } from '../af-store-doc';

export class Article extends AfStoreDoc {
  name: string;
  price: number;
  description?: string;
  order: number;
  disabled: boolean;
  descStyle: string;
}
