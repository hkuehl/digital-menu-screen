import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CurrencyPipe, registerLocaleData } from '@angular/common';
import localDe from '@angular/common/locales/de';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ColumnComponent } from './column/column.component';
import { CategoryComponent } from './category/category.component';
import { ArticleComponent } from './article/article.component';
import { PromotionComponent } from './promotion/promotion.component';

registerLocaleData(localDe, 'de-DE');

@NgModule({
  declarations: [
    AppComponent,
    ColumnComponent,
    CategoryComponent,
    ArticleComponent,
    PromotionComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  providers: [
    CurrencyPipe,
    {
      provide: LOCALE_ID,
      useValue: 'de-DE'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
