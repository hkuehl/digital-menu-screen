import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Screen } from './screen';
import { Column } from './column/column';
import { Promotion } from './promotion/promotion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  backgroundHeight: number;

  private screenId: string;
  screen: Observable<Screen>;
  columns: Observable<Column[]>;
  promotions: Observable<Promotion[]>;
  promLength: number;
  promotionList: Promotion[];

  sinceLogo = require('../assets/since_1980.png');
  backgroundImage = require('../assets/background_gray_stone.jpg');
  headerImage = require('../assets/Titel.png');

  constructor(private afStore: AngularFirestore) {
    this.backgroundHeight = window.innerHeight;
  }

  ngOnInit(): void {
    this.screenId = this.getQueryVariable('id');
    console.log(this.screenId);
    if (this.screenId === '') {
      return;
    }
    this.screen = this.afStore.doc<Screen>(`screens/${this.screenId}`).snapshotChanges().pipe(map(
      screen => {
        if (!screen.payload.exists) {
          return;
        }
        const id = screen.payload.id;
        const data = screen.payload.data() as Screen;
        const path = screen.payload.ref.path;
        return {id, path, ...data};
      }
    ));

    this.columns = this.afStore.collection<Column>(`screens/${this.screenId}/columns`,
      ref => ref.where('disabled', '==', false).orderBy('order'))
      .snapshotChanges().pipe(map(
        snapshot => {
          return snapshot.map(column => {
              const id = column.payload.doc.id;
              const data = column.payload.doc.data() as Column;
              const path = column.payload.doc.ref.path;
              return {id, path, ...data};
            }
          );
        }
      ));

    this.promotions = this.afStore.collection<Promotion>(`screens/${this.screenId}/promotions`,
        ref => ref.where('disabled', '==', false).orderBy('order'))
      .snapshotChanges().pipe(map(
        snapshot => {
          return snapshot.map(promotion => {
              const id = promotion.payload.doc.id;
              const data = promotion.payload.doc.data() as Promotion;
              data.position = data.position ? data.position : 'topLeft';
              const path = promotion.payload.doc.ref.path;
              return {id, path, ...data};
            }
          );
        }
      ));

    this.promotions.subscribe(promotions => {
      this.promLength = promotions.length;
      this.promotionList = promotions;
    });
  }

  private getQueryVariable(variable: string): string {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      if (pair[0] === variable) {
        return pair[1];
      }
    }
    return ('');
  }

  getPromotionFontSize(text: string): number {
    const charAmount = text.replace(' ', '').length;
    if (charAmount > 100) {
      return 3.3;
    }
    if (charAmount > 95) {
      return 3.4;
    }
    if (charAmount > 90) {
      return 3.5;
    }
    if (charAmount > 85) {
      return 3.6;
    }
    if (charAmount > 80) {
      return 3.7;
    }
    if (charAmount > 75) {
      return 3.8;
    }
    if (charAmount > 70) {
      return 3.9;
    }
    if (charAmount > 65) {
      return 4;
    }
    if (charAmount > 60) {
      return 4.1;
    }
    if (charAmount > 55) {
      return 4.2;
    }
    if (charAmount > 50) {
      return 4.3;
    }
    if (charAmount > 45) {
      return 4.4;
    }
    if (charAmount > 40) {
      return 4.5;
    }
    if (charAmount > 35) {
      return 4.6;
    }
    if (charAmount > 30) {
      return 4.7;
    }
    if (charAmount > 25) {
      return 4.8;
    }
    if (charAmount > 20) {
      return 4.9;
    }
    if (charAmount > 15) {
      return 5;
    }
    if (charAmount > 10) {
      return 5.1;
    }
    if (charAmount > 5) {
      return 5.2;
    }
    if (charAmount > 0) {
      return 5.3;
    }
  }

  paddingTop(first: boolean, last: boolean): boolean {
    if (!this.promotionList || this.promotionList.length < 1) {
      return false;
    }

    const promPos = this.promotionList[0].position;
    if (promPos === 'topLeft' && first) {
      return true;
    }

    if (promPos === 'topRight' && last) {
      return true;
    }

    return false;
  }
}
