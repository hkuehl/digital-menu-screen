import { AfStoreDoc } from '../af-store-doc';

export class Column extends AfStoreDoc {
  name: string;
  order: number;
  disabled: boolean;
}
