import { Component, Input, OnInit } from '@angular/core';
import { Column } from './column';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Category } from '../category/category';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {
  @Input() column: Column;
  categories: Observable<Category[]>;

  constructor(private afStore: AngularFirestore) { }

  ngOnInit() {
    this.categories = this.afStore.doc(this.column.path).collection('categories',
        ref => ref.where('disabled', '==', false).orderBy('order')).snapshotChanges().pipe(map(
      snapshot => {
        return snapshot.map(category => {
          const id = category.payload.doc.id;
          const data = category.payload.doc.data() as Category;
          const path = category.payload.doc.ref.path;
          return {id, path, ...data};
        });
      }
    ));
  }

}
