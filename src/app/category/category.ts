import { AfStoreDoc } from '../af-store-doc';

export class Category extends AfStoreDoc {
  name: string;
  order: number;
  disabled: boolean;
  iconClass?: string;
  iconPath?: string;
  description?: string;
}
