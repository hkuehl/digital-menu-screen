import { Component, Input, OnInit } from '@angular/core';
import { Category } from './category';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Article } from '../article/article';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  @Input() category: Category;
  articles: Observable<Article[]>;

  constructor(private afStore: AngularFirestore) { }

  ngOnInit() {
    this.articles = this.afStore.doc(this.category.path).collection('articles',
        ref => ref.where('disabled', '==', false).orderBy('order')).snapshotChanges().pipe(map(
      snapshot => {
        return snapshot.map(article => {
          const id = article.payload.doc.id;
          const data = article.payload.doc.data() as Article;
          if (!data.descStyle) {
            data.descStyle = 'under';
          }
          const path = article.payload.doc.ref.path;
          return {id, path, ...data};
        });
      }
    ));
  }

}
