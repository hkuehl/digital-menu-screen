import { AfStoreDoc } from '../af-store-doc';

export class Promotion extends AfStoreDoc {
  name: string;
  text: string;
  position: string;
}
