// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDm2mSbneVm5srBQThLJdorrmX9XPuATAA',
    authDomain: 'digital-menu-3fe0b.firebaseapp.com',
    databaseURL: 'https://digital-menu-3fe0b.firebaseio.com',
    projectId: 'digital-menu-3fe0b',
    storageBucket: 'digital-menu-3fe0b.appspot.com',
    messagingSenderId: '1096412169379',
    appId: '1:1096412169379:web:a1c18e577e244218'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
